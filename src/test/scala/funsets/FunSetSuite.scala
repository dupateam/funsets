package funsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {

  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
   test("string take") {
     val message = "hello, world"
     assert(message.take(5) == "hello")
   }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
   test("adding ints") {
     assert(1 + 2 === 3)
   }


  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }

  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   *
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   *
   *   val s1 = singletonSet(1)
   *
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   *
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   *
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   *
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {

    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3".
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements of each set") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  test("intersect contains shared elements") {
    new TestSets {
      val u1 = union(s1, s2)
      val u2 = union(s1, s3)
      val i1 = intersect(u1,u2)
      assert(contains(i1, 1), "Intersects 1")
      assert(!contains(i1, 2), "Not intersects 2")
      assert(!contains(i1, 3), "Not intersects 3")
    }
  }


  test("diff should contain elements that are in the first set, but not in the other") {
    new TestSets {
      val u1 = union(s1, s2)
      val u2 = union(s1, s3)
      val d1 = diff(u1,u2)
      assert(!contains(d1, 1), "Not diff due to presence in the first set")
      assert(contains(d1, 2), "Diff 2")
      assert(!contains(d1, 3), "Not diff due to presence in the other set")
    }
  }

  test("filter should be applied") {
    new TestSets {
      val u1 = union(s1, s2)
      val filtered = filter(u1,(elem: Int) => elem == 2 )

      assert(!contains(filtered, 1), "Filtered out")
      assert(contains(filtered, 2), "Fulfils filter")
      assert(!contains(filtered, 3), "Filtered out")
    }
  }

  test("check for all predicate") {
    new TestSets {
      val u1 = union(s1, s2)
      val r1 = forall(u1,(elem: Int) => elem > 0 )
      val r2 = forall(u1,(elem: Int) => elem > 1 )

      assert(r1 === true, "All are greather than 0")
      assert(r2 === false, "Not all are greather than 1")

    }
  }

  test("check for at least one predicate") {
    new TestSets {
      val u1 = union(s1, s2)
      val r1 = exists(u1,(elem: Int) => elem > 1 )
      val r2 = exists(u1,(elem: Int) => elem == 0 )

      assert(r1 === true, "At least one greather than 1")
      assert(r2 === false, "There is not element that is eq 0")

    }
  }

  test("map set to another set by applying transformation function on each element   ") {
    new TestSets {
      val u1 = union(s1, s2)
      val r1 = map(u1,x => x * 10 )

      assert(!contains(u1,1) === false, "Does not contains non transformed item: 1")
      assert(!contains(u1,2) === false, "Does not contains non transformed item: 2")
      assert(contains(r1,10) === true, "Contains transformed item: 10")
      assert(contains(r1,20) === true, "Contains transformed item: 20")


    }
  }
}
